from django.core.management.base import BaseCommand

from reimbursements.integrations.rt import RT
from reimbursements.models import Request


class Command(BaseCommand):
    help = "Create a test RT ticket"

    def add_arguments(self, parser):
        parser.add_argument(
            "--request",
            metavar="ID",
            type=int,
            required=True,
            help="Request to create an RT ticket for",
        )

    def handle(self, *args, **options):
        request = Request.objects.get(id=options["request"])
        group = request.payer_group
        rt = RT(group)
        ticket = rt.create_ticket(request, RT.ticket_body(request))
        print("Created ticket", ticket)
