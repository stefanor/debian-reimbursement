from decimal import Decimal

from django import forms
from django.utils.translation import gettext_lazy as _

from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Fieldset, Layout
from historical_currencies.choices import currency_choices
from historical_currencies.formatting import render_amount


class RequestReimbursementForm(forms.Form):
    details = forms.CharField(
        help_text=_("An optional comment to attach to the reimbursement"),
        required=False,
        widget=forms.Textarea(attrs={"rows": 3}),
    )
    fee = forms.DecimalField(
        help_text=_("Reimbursement Cost: Bank transfer fees etc."),
        required=False,
    )
    fee_currency = forms.ChoiceField(
        choices=currency_choices(),
        help_text=_("Reimbursement Cost: Currency"),
        required=False,
    )
    fee_details = forms.CharField(
        help_text=_("An optional comment to attach to the fees"),
        required=False,
        widget=forms.Textarea(attrs={"rows": 3}),
    )

    def __init__(self, request, **kwargs):
        super().__init__(**kwargs)
        self.type_fields = []
        for type_id, summary in request.summarize_by_type().items():
            if type_id == "total":
                continue
            default = summary.reimburseable
            field = forms.DecimalField(
                label=summary.name,
                initial=default,
                help_text=_("Budgeted: %s, Spent: %s, Approved up to: %s")
                % (
                    render_amount(*summary.qualified_requested),
                    render_amount(*summary.qualified_spent),
                    render_amount(*summary.qualified_approved),
                ),
                widget=forms.widgets.NumberInput(attrs={"class": "sum-line"}),
            )
            self.fields[str(type_id)] = field
            self.type_fields.append(str(type_id))
        self.fields["fee_currency"].initial = request.currency
        self.helper = FormHelper(self)
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Fieldset(_("Expense Types"), *self.type_fields),
            "details",
            HTML(
                "<legend data-bs-toggle='collapse' data-bs-target='#fees'>"
                + "<i class='bi bi-caret-right-fill'></i> "
                + _("Fees")
                + "</legend>"
            ),
            Fieldset(
                None,
                "fee",
                "fee_currency",
                "fee_details",
                css_id="fees",
                css_class="collapse",
            ),
        )

    def clean(self):
        cleaned_data = super().clean()
        fee = cleaned_data.get("fee")
        fee_currency = cleaned_data.get("fee_currency")
        if fee and not fee_currency:
            self.add_error("fee_currency", _("Currency required"))

        total = sum(Decimal(cleaned_data[field]) for field in self.type_fields)
        if total == Decimal(0):
            for field in self.type_fields:
                self.add_error(field, _("No expense types have been reimbursed."))
