from collections import defaultdict
from decimal import Decimal

from django.conf import settings
from django.db import models


class SoftDeletionManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(deleted=False)


class RequestType(models.Model):
    name = models.CharField(max_length=50)
    deleted = models.BooleanField(default=False)

    objects = SoftDeletionManager()
    all_objects = models.Manager()

    def __str__(self):
        return self.name

    @classmethod
    def expense_type_map(cls):
        output = defaultdict(list)
        for row in RequestType.expense_types.through.objects.all():
            output[row.requesttype_id].append(row.expensetype_id)
        return output


class ExpenseType(models.Model):
    request_types = models.ManyToManyField(RequestType, related_name="expense_types")
    name = models.CharField(max_length=200, unique=True)
    deleted = models.BooleanField(default=False)

    objects = SoftDeletionManager()
    all_objects = models.Manager()

    def __str__(self):
        return self.name


class ExpenseTypeAllocation:
    def __init__(
        self, name, currency, requested=0, spent=0, reimbursed=0, special=False
    ):
        self.name = name
        self.currency = currency
        self.requested = Decimal(requested)
        self.spent = Decimal(spent)
        self.reimbursed = Decimal(reimbursed)
        self.special = special

    @property
    def qualified_requested(self):
        return (self.requested, self.currency)

    @property
    def qualified_spent(self):
        return (self.spent, self.currency)

    @property
    def qualified_reimbursed(self):
        return (self.reimbursed, self.currency)

    @property
    def remaining(self):
        return self.requested - self.spent

    @property
    def qualified_remaining(self):
        return (self.remaining, self.currency)

    @property
    def over_budget(self):
        return self.spent > self.requested

    @property
    def within_budget(self):
        return not self.over_budget

    @property
    def approved(self):
        if self.within_budget:
            return self.requested
        return self.requested * (
            1 + Decimal(settings.OVERBUDGET_LEEWAY_PERCENTAGE) / 100
        )

    @property
    def reimburseable(self):
        return min(self.approved, self.spent)

    @property
    def qualified_approved(self):
        return (self.approved, self.currency)

    @property
    def qualified_reimburseable(self):
        return (self.reimburseable, self.currency)

    def to_dict(self):
        return {
            "currency": self.currency,
            "requested": float(self.requested),
            "spent": float(self.spent),
            "reimbursed": float(self.reimbursed),
        }
