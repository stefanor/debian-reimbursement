from django.conf import settings
from django.contrib.auth.models import Group
from django.db import models

from reimbursements.models.request import Request


class RTTicket(models.Model):
    request = models.ForeignKey(
        Request, on_delete=models.CASCADE, related_name="rt_tickets"
    )
    # We duplicate payer_group here so that tickets get retained even if
    # requests bounce between payers
    payer_group = models.ForeignKey(
        Group,
        on_delete=models.PROTECT,
    )
    ticket_id = models.IntegerField()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["request", "payer_group"], name="One RT Ticket per payer_group"
            ),
            models.UniqueConstraint(
                fields=["payer_group", "ticket_id"], name="One Request per RT Ticket"
            ),
        ]

    @property
    def url(self):
        instance = settings.RT_INSTANCES[self.payer_group.name]
        return f"{instance['url']}/Ticket/Display.html?id={self.ticket_id}"
