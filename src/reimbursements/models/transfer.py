from django.conf import settings
from django.db import models
from django.urls import reverse
from django.utils import timezone

from reimbursements.models.request import Request


class RequestTransferManager(models.Manager["RequestTransfer"]):
    def get_by_token(self, token: str) -> "RequestTransfer":
        return self.get_queryset().get(token=token, expires__gt=timezone.now())


class RequestTransfer(models.Model):
    request = models.OneToOneField(
        Request, on_delete=models.CASCADE, related_name="transfer"
    )
    created = models.DateTimeField(auto_now_add=True)
    expires = models.DateTimeField()
    token = models.CharField(max_length=20, unique=True)

    objects = RequestTransferManager()

    def get_absolute_url(self) -> str:
        return reverse("request-transfer-claim", kwargs={"token": self.token})

    def get_fully_qualified_url(self) -> str:
        return settings.SITE_URL.rstrip("/") + self.get_absolute_url()
