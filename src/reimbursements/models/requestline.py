from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _

from reimbursements.models.types import ExpenseType
from reimbursements.models.request import Request


class RequestLine(models.Model):
    request = models.ForeignKey(Request, on_delete=models.CASCADE, related_name="lines")
    expense_type = models.ForeignKey(ExpenseType, on_delete=models.PROTECT)
    description = models.TextField()
    amount = models.DecimalField(decimal_places=2, max_digits=15)

    @property
    def qualified_amount(self):
        return (self.amount, self.request.currency)

    def clean(self):
        super().clean()
        request_type_id = self.request.request_type_id
        if (
            request_type_id
            and self.expense_type_id
            and not self.expense_type.request_types.filter(pk=request_type_id)
        ):
            raise ValidationError(
                {
                    "expense_type": _("%s is not a valid expense type for %s requests")
                    % (self.expense_type.name, self.request.request_type.name),
                }
            )
