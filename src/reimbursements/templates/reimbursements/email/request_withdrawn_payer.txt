{% load i18n %}
{% block subject %}
{% blocktrans with subject=object.subject %}
{{ SITE_NAME }}: Request withdrawn: {{ subject }}
{% endblocktrans %}
{% endblock %}
{% block body %}
{% blocktrans with payer_group=object.payer_group.name subject=object.subject requester_name=object.requester_name %}
Dear {{ payer_group }} member,

The request submitted by {{ requester_name }} for {{ subject }}
has been withdrawn.

{% endblocktrans %}
{% if details %}{% blocktrans %}Note: {{ details }}{% endblocktrans %}{% endif %}

{{ object.get_fully_qualified_url }}
{% endblock %}
