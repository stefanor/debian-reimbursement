{% load i18n %}
{% block subject %}
{% blocktrans with subject=object.subject %}
{{ SITE_NAME }}: Request sent back: {{ subject }}
{% endblocktrans %}
{% endblock %}
{% block body %}
{% blocktrans with payer_group=object.payer_group.name subject=object.subject requester_name=object.requester_name actor_name=actor.get_full_name|default:actor.username %}
Dear {{ payer_group }} member,

The request submitted by {{ requester_name }} for {{ subject }}
has been sent back for more information by {{ actor_name }}.

Details:
{{ details }}
{% endblocktrans %}

{{ object.get_fully_qualified_url }}
{% endblock %}
