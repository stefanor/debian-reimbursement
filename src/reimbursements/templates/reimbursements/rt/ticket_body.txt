{% load i18n %}
{% load currency_format %}
{% blocktrans with request_id=request.id request_url=request.get_fully_qualified_url total_budget=request.total|currency %}
{{ SITE_NAME }} Request {{ request_id }}
{{ request_url }}
Description:
{{ details }}

Total Approved: {{ total_budget }}
From:
{% endblocktrans %}{% for type in request.summarize_by_type.values %}{% if not type.special %}
* {{ type.name }}: {{ type.qualified_requested|currency }}{% endif %}{% endfor %}{% blocktrans with total_receipts=request.receipts_total|currency %}

Total Receipts: {{ total_receipts }}
From:
{% endblocktrans %}{% for type in request.summarize_by_type.values %}{% if not type.special %}
* {{ type.name }}: {{ type.qualified_spent|currency }}{% endif %}{% endfor %}{% blocktrans with total_reimburseable=request.total_reimburseable|currency %}

====================================
Total Reimburseable: {{ total_reimburseable }}
====================================

This RT request is for auditing purposes only.
Please discuss this request on {{ SITE_NAME }} if possible, not RT.
{% endblocktrans %}
