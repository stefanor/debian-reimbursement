from django.db import transaction
from django.http import HttpResponseNotAllowed
from django.shortcuts import redirect
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView

from reimbursements.forms.requestline import RequestLineFormset


class FormAndSetMixin:
    """
    A Class Based View mixin for a primary form with dependent formset

    Basically, a fairly minimal thing that combines with CreateView/UpdateView
    """

    def get_formset_class(self):
        """
        Returns the formset class to use in this view
        """
        return self.formset_class

    def get_formset(self, formset_class, instance=None):
        """
        Returns an instance of the formset to be used in this view.
        Optionally takes the takes the instance of the parent object.
        """
        kwargs = self.get_form_kwargs()
        if instance:
            kwargs["instance"] = instance
        return formset_class(**kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if "formset" not in context:
            context["formset"] = RequestLineFormset()
        return context

    def get(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        formset_class = self.get_formset_class()
        formset = self.get_formset(formset_class)
        return self.render_to_response(
            self.get_context_data(form=form, formset=formset)
        )

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            self.object = form.save(commit=False)
        formset_class = self.get_formset_class()
        formset = self.get_formset(formset_class, instance=self.object)
        if self.is_valid(form, formset):
            return self.forms_valid(form, formset)
        else:
            return self.forms_invalid(form, formset)

    def is_valid(self, form, formset):
        return form.is_valid() and formset.is_valid()

    def form_valid(self, form):
        raise NotImplementedError("Should be calling forms_valid()")

    def form_invalid(self, form):
        raise NotImplementedError("Should be calling forms_invalid()")

    def forms_invalid(self, form, formset):
        return self.render_to_response(
            self.get_context_data(form=form, formset=formset)
        )

    def forms_valid(self, form, formset):
        with transaction.atomic():
            self.object.save()
            formset.save()
        return self.get_success_url()

    def get_success_url(self):
        return redirect(self.object.get_absolute_url())


class FormAndSetCreateView(FormAndSetMixin, CreateView):
    """A form+formset CreateView"""

    def get(self, request, *args, **kwargs):
        self.object = None
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = None
        return super().post(request, *args, **kwargs)


class FormAndSetUpdateView(FormAndSetMixin, UpdateView):
    """A form+formset UpdateView"""

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().post(request, *args, **kwargs)


class DetailActionView(DetailView):
    """A DetailView for performing actions only"""

    def get(self, request, *args, **kwargs):
        # DetailView provides a get()
        return HttpResponseNotAllowed(["post"])
