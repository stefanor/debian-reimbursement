from django import forms
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit


class UserEditForm(forms.Form):
    username = forms.CharField(max_length=150, required=True)
    name = forms.CharField(max_length=300, required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.add_input(Submit("submit", _("Update")))

    def clean_username(self):
        username = self.cleaned_data["username"]
        if "username" in self.changed_data:
            User = get_user_model()
            if User.objects.filter(username=username).exists():
                raise ValidationError(
                    f"A user with username {username} already exists."
                )

        return username
