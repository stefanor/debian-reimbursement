import json
from argparse import ArgumentParser
from pathlib import Path
from tempfile import TemporaryDirectory

from werkzeug.exceptions import BadRequest, HTTPException
from werkzeug.routing import Map, Rule
from werkzeug.serving import run_simple
from werkzeug.utils import secure_filename, send_file
from werkzeug.wrappers import Request, Response

from image_wrangler.pdf_bundler import bundle_pdf
from image_wrangler.thumbnailer import thumbnail
from image_wrangler.validator import InvalidImage, validate_img


def mime_to_pil_type(mimetype: str) -> str:
    bad_type = BadRequest(description="Unexpected content-type. Only images accepted.")
    if mimetype.startswith(("image/", "application/")):
        pil_type = mimetype.split("/", 1)[1].upper()
    else:
        raise bad_type
    if pil_type in ("JPEG", "PDF", "PNG", "WEBP"):
        return pil_type
    raise bad_type


class ImageWranglerServer:
    def root(self, request, args):
        return Response(
            "Thumbnail Generator\n"
            "Post images to /thumbnail/<output-format>/<width>x<height>/\n"
            "PDF Compiler\n"
            "Post an instruction manifest to /bundle-pdf/\n"
        )

    def thumbnailer(self, request, args):
        if request.method != "POST":
            raise BadRequest(description="Only POST accepted")
        input_type = mime_to_pil_type(request.mimetype)

        response = Response(
            mimetype=f"image/{args['output_type'].lower()}",
        )

        thumbnail(
            input_img=request.stream,
            input_type=input_type,
            output_img=response.stream,
            output_type=args["output_type"],
            max_dimensions=(args["width"], args["height"]),
        )
        return response

    def validate(self, request, args):
        if request.method != "POST":
            raise BadRequest(description="Only POST accepted")
        pil_type = mime_to_pil_type(request.mimetype)
        try:
            validate_img(img=request.stream, pil_type=pil_type)
        except InvalidImage as e:
            return Response(status=400, response=str(e))
        return Response(status=204)

    def pdf_bundler(self, request, args):
        if request.method != "POST":
            raise BadRequest(description="Only POST accepted")
        if request.mimetype != "multipart/form-data":
            raise BadRequest(
                description=(
                    "Unexpected content-type. Only multipart/form-data accepted."
                )
            )

        manifest = json.loads(request.form["manifest"])
        with TemporaryDirectory(prefix="pdf-bundle") as temp_dir:
            temp_path = Path(temp_dir)
            for name, file in request.files.items():
                secure_name = secure_filename(name)
                if name != secure_name:
                    for item in manifest["content"]:
                        if item.get("filename", None) == name:
                            item["filename"] = secure_name
                with open(temp_path / secure_name, "wb") as f:
                    file.save(f)
                file.close()
            result = bundle_pdf(manifest, temp_path)

            f = result.open("rb")
            # send_file will close f.
            return send_file(f, request.environ, mimetype="application/pdf")

    def url_map(self):
        return Map(
            [
                Rule("/", endpoint=self.root),
                Rule("/validate/", endpoint=self.validate),
                Rule(
                    "/thumbnail/<output_type>/<int:width>x<int:height>/",
                    endpoint=self.thumbnailer,
                ),
                Rule(
                    "/bundle-pdf/",
                    endpoint=self.pdf_bundler,
                ),
            ]
        )

    def wsgi_app(self, environ, start_response):
        request = Request(environ)
        urls = self.url_map().bind_to_environ(environ)
        try:
            endpoint, args = urls.match()
            response = endpoint(request, args)
        except HTTPException as e:
            return e(environ, start_response)

        return response(environ, start_response)

    def __call__(self, environ, start_response):
        return self.wsgi_app(environ, start_response)


def main():
    p = ArgumentParser("Reimbursements Image Wrangler Server")
    p.add_argument("--hostname", default="localhost", help="Hostname / IP to listen on")
    p.add_argument("--port", "-p", type=int, default=2303, help="Port to listen on")
    p.add_argument("--debug", "-d", action="store_true", help="Use debugger")
    p.add_argument("--reloader", "-r", action="store_true", help="Use auto-reloader")
    p.add_argument(
        "--threaded",
        action="store_true",
        help="Allow concurrent requests using threads",
    )
    args = p.parse_args()

    app = ImageWranglerServer()

    run_simple(
        args.hostname,
        args.port,
        app,
        use_debugger=args.debug,
        use_reloader=args.reloader,
        threaded=args.threaded,
    )


if __name__ == "__main__":
    main()
