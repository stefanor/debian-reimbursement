from pathlib import Path
from tempfile import NamedTemporaryFile, TemporaryDirectory
from unittest import TestCase, mock

from PIL import Image

from image_wrangler.pdf_bundler import PDFBundler, bundle_pdf


class TestBundlePdf(TestCase):
    @mock.patch("image_wrangler.pdf_bundler.PDFBundler", autospec=True)
    def test_bundle_pdf(self, MockPDFBundler):
        manifest = {"foo": "bar"}
        workdir = Path("/foo")
        bundle_pdf(manifest, workdir)
        MockPDFBundler.assert_called_once_with(manifest, workdir)
        instance = MockPDFBundler.return_value
        instance.bundle.assert_called_once_with()


class TestPDFBundlerInternals(TestCase):
    def setUp(self):
        self.manifest = {"content": ["foo"], "foo": "bar"}
        self.workdir = Path("/baz")
        self.bundler = PDFBundler(self.manifest, self.workdir)

    def create_image(self) -> Path:
        im = Image.new("RGBA", (32, 32), "white")
        file_ = NamedTemporaryFile(delete=False)
        path = Path(file_.name)
        self.addCleanup(path.unlink)
        im.save(file_, format="PNG")
        return path

    def test_instantiation(self):
        self.assertEqual(self.bundler.manifest, self.manifest)
        self.assertEqual(self.bundler.workdir, self.workdir)

    def test_parse_manifest(self):
        self.bundler.parse_manifest()
        self.assertEqual(self.bundler._content, self.manifest["content"])

    @mock.patch("image_wrangler.pdf_bundler.check_call", autospec=True)
    def test_html_to_pdf(self, mock_check_call):
        url = "https://example.com/page"
        access_key = "secret_key"
        output_path = Path("/output")
        self.bundler.html_to_pdf(url, access_key, output_path)

        mock_check_call.assert_called_once_with(
            [
                "wkhtmltopdf",
                "--custom-header",
                "Authorization",
                f"Bearer {access_key}",
                "--custom-header-propagation",
                "--run-script",
                "prepareToPrint()",
                "--debug-javascript",
                "--quiet",
                url,
                str(output_path),
            ]
        )

    def test_image_to_pdf(self):
        input_ = self.create_image()
        with NamedTemporaryFile() as output:
            self.bundler.image_to_pdf(input_, "PNG", Path(output.name))
            self.assertEqual(output.read(4), b"%PDF")

    @mock.patch("image_wrangler.pdf_bundler.check_call", autospec=True)
    def test_concatenate_pdfs(self, mock_check_call):
        self.bundler.concatenate_pdfs([Path("foo"), Path("bar")], Path("output"))

        mock_check_call.assert_called_once_with(
            [
                "pdfunite",
                "foo",
                "bar",
                "output",
            ]
        )


class TestPDFBundlerWithWorkdir(TestCase):
    def setUp(self):
        self.manifest = {
            "content": [
                {"filename": "a", "pil_type": "PNG"},
                {"filename": "b", "pil_type": "PDF"},
                {"url": "https://example.com/c", "access_key": "secret"},
            ],
        }
        self.workdir_tempdir = TemporaryDirectory(prefix="test_pdf_bundler")
        self.workdir = Path(self.workdir_tempdir.name)

    def test_bundle(self):
        bundler = PDFBundler(self.manifest, self.workdir)
        with (
            mock.patch.object(bundler, "html_to_pdf", autospec=True) as html_to_pdf,
            mock.patch.object(bundler, "image_to_pdf", autospec=True) as image_to_pdf,
            mock.patch.object(
                bundler, "concatenate_pdfs", autospec=True
            ) as concatenate_pdfs,
        ):
            self.assertEqual(bundler.bundle(), self.workdir / "bundle.pdf")
        image_to_pdf.assert_called_once_with(
            self.workdir / "a", "PNG", self.workdir / "converted" / "0.pdf"
        )
        html_to_pdf.assert_called_once_with(
            "https://example.com/c", "secret", self.workdir / "converted" / "2.pdf"
        )
        concatenate_pdfs.assert_called_once_with(
            [
                self.workdir / "converted" / "0.pdf",
                self.workdir / "b",
                self.workdir / "converted" / "2.pdf",
            ],
            self.workdir / "bundle.pdf",
        )

    def test_bundle_unknown_image(self):
        self.manifest["content"] = [{"filename": "foo", "pil_type": "BAR"}]
        bundler = PDFBundler(self.manifest, self.workdir)
        with self.assertRaises(ValueError):
            bundler.bundle()
